// Config
var SERVER_URL = "localhost";
var PORT = 3000;

SERVER_URL = "http://" + SERVER_URL + ":" + PORT + "/match";

// Constants
var WIDTH_STAGE = 700,
    HEIGHT_STAGE = 700,
    BG_BORDERWEIGHT = 10,
    WIDTH_GUN = 40,
    HEIGHT_GUN = 40,
    WIDTH_CHARACTER = 20,
    HEIGHT_CHARACTER = 20,
    WIDTH_GOAL = 35,
    HEIGHT_GOAL = 20,
    WIDTH_ROCK = 30,
    HEIGHT_ROCK = 30;

var gameOptions = {
	renderer: Kiwi.RENDERER_CANVAS, 
	width: WIDTH_STAGE,
	height: HEIGHT_STAGE
};

var myGame = new Kiwi.Game('content', 'myGame', null, gameOptions);
var actual = {};
var myState = [];
var empty = new Kiwi.State("empty");
myGame.states.addState(empty);

var nGame = -1; // Number of games
var numRocks = 0; // Number of rocks
var processing = false; // Procession score table
var adjust = 0; // Adjust array from other characters
var counter = 0; // Counters of game
var data = {}; // Data for server
var idRol = -1; // Rol ID
var name = ""; // User's name
var characters = []; // All other characters
var numPlayers = 0; // Number of players in game
var isDead = false; // Am i dead?
var start = false; // Start game

// Start counter
function startCounter(is) {
    endCounter();
    counter = setInterval(function(){
        time--;
        if(is == "join" || is == "start") $("#timeToJoin").text(time);
        if(is == "wait") $("#waitBox .text").text(time);
        console.log(time);
    }, 1000);
}

// End counter
function endCounter() {
    clearInterval(counter);
}

$(function() {
    // Match is the "socket"
    var match = io.connect(SERVER_URL);

    // Login
    $('.button').on('click', function() {
        match.emit('login', $('#msg').val());
        console.log($('#msg').val());
        name = $('#msg').val();
        $('#msg').val('');
    });

    // Join a game
    $('.button2').on('click', function() {
        match.emit('joinGame', name);
        $(this).attr("disabled", true);
    });

    // Receive message from server for start counter
    match.on('startCounter', function(resp) {
        time = resp.time;
        startCounter(resp.is);
        if(resp.is == "join") {
            $("#textTime").text("Tiempo para entrar al Juego: ");
            $('#timeToJoin').text(time);
        }
        if(resp.is == "wait") {
            $('#waitBox .text').text(time);
            $('#waitBox').addClass("active");
        }
        if(resp.is == "start") {
            $("#textTime").text("Tiempo: ");
            $('#timeToJoin').text(time);
        }
    });

    // Receive message from server for end counter
    match.on('endCounter', function() {
        endCounter();
        $('#waitBox').removeClass("active");
    });

    // Any message from server to show
    match.on('alert', function(msg) {
        if(msg == "Numero de Jugadores menor de 3") {
            $("#textTime").text("Numero de Jugadores menor de 3");
            $('#timeToJoin').text("");
            $('.button2').attr("disabled", false);
        }
    });

    // If login success
    match.on('loginSuccess', function() {
        $('.button').attr("disabled", true);
        $('.button2').attr("disabled", false);
        $('#msg').attr("disabled", true);
    });

    // New game. Receive data
    match.on('newGame', function(inf){
        rocksCoor = inf.rocksCoor;
        numPlayers = inf.numPlayers;
        idRol = inf.id;
        console.log("IdRol: " + idRol);
    });

    // Start game. Start Canvas
    match.on('startGame', function(time) {
        if(idRol != -1) {
            startCanvas();
            setTimeout(function() {
                start = true;
            }, time * 1000);
        }
        else console.log("idRol: " + idRol);
    });

    // Players' update
    match.on("update", function(positions) {
        if(actual != null) { // If the state exist
            adjust = 0;
            // Update positions and update dead
            for(var i = 1; i < positions.characters.length; i++) {
                if(idRol == i && i != 1) adjust = 1;
                if(characters[i-1-adjust] != undefined) {
                    if(idRol != i && !positions.characters[i].isDead) {
                        characters[i-1-adjust].x = positions.characters[i].x;
                        characters[i-1-adjust].y = positions.characters[i].y;
                    }
                    if(positions.characters[i].isDead) {
                        if(idRol != i) {
                            characters[i-1-adjust].x = -50;
                            characters[i-1-adjust].y = -50;    
                        }
                        else {
                            actual.character.x = -50;
                            actual.character.y = -50;
                            isDead = true;
                        }
                    }
                }
                if(idRol == i && i == 1) adjust = 1;
            }
            if(actual.gun != undefined) {
                actual.gun.x = positions.gun.x;
                actual.gun.y = positions.gun.y;
            }
        }
    });

    // Receive if the gun is loaded
    match.on("isLoaded", function(isLoaded) {
        console.log("Esta Cargada: ", isLoaded);
        if(idRol != -1 && idRol != 0 && actual.gun != undefined) {
            if(isLoaded) actual.gun.animation.play("loaded");
            else actual.gun.animation.play("downloaded");
        }
    });

    // Receive the score table
    match.on("updateScore", function(scoreTable) {
        console.log(scoreTable);
        $('#scoreTable').html('');
        for(var i = 0; i < scoreTable.length; i++) {
            $('#scoreTable').append('<li><span>' + scoreTable[i].name + ': </span>' + scoreTable[i].score + '</li>');
        }
        processing = false;
    });

    // Receive the game over
    match.on("gameOver", function() {
        console.log("GameOver");
        start = false;
        isDead = true;
        myGame.states.switchState("empty");
        actual = undefined;
        $("#textTime").text("Juego Terminado");
        $('#timeToJoin').text("");
        $('.button2').attr("disabled", false);
    });

    // Start canvas and game (Graphics)
    function startCanvas() {

        isDead = false;

        myState[++nGame] = new Kiwi.State("myState" + nGame);
        actual = myState[nGame];

        // Preload the assets
        actual.preload = function () {
            Kiwi.State.prototype.preload.call(this);

            this.addImage('background', 'assets/background.png');
            this.addSpriteSheet('gunSprite', 'assets/gunSheet.png', WIDTH_GUN, HEIGHT_GUN);
            this.addImage('character', 'assets/character.png');
            this.addImage('otherCharacter', 'assets/other.png');
            this.addImage('goal', 'assets/goal.png');
            this.addImage('rock', 'assets/rock.png');
        }

        actual.create = function() {
            Kiwi.State.prototype.create.call(this);

            // Config
            if(idRol == 0) this.mouse = this.game.input.mouse;
            this.game.time.clock.units = 250; // Default: 1000
            this.upKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.W);
            this.leftKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.A);
            this.downKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.S);
            this.rightKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.D);

            // Background
            this.background = new Kiwi.GameObjects.StaticImage(this, this.textures.background, 0, 0);

            // Goal
            this.goal = new Kiwi.GameObjects.StaticImage(this, this.textures.goal, (WIDTH_STAGE/2 - WIDTH_GOAL/2), (HEIGHT_STAGE - HEIGHT_GOAL - 15));

            // Gun
            this.gun = new Kiwi.GameObjects.Sprite(this, this.textures.gunSprite, 350, 350);
            this.gun.limits = {};
            this.gun.limits.x = {};
            this.gun.limits.y = {};
            this.gun.limits.x.min = BG_BORDERWEIGHT;
            this.gun.limits.x.max = WIDTH_STAGE - BG_BORDERWEIGHT - WIDTH_GUN;
            this.gun.limits.y.min = BG_BORDERWEIGHT;
            this.gun.limits.y.max = HEIGHT_STAGE - BG_BORDERWEIGHT - HEIGHT_GUN;
            this.gun.animation.add("loaded", [0,1], 0.5, true);
            this.gun.animation.add("downloaded", [2,3], 0.5, true);
            this.gun.animation.play("loaded");

            // Character
            this.character = new Kiwi.GameObjects.StaticImage(this, this.textures.character, 100, 20);
            this.character.limits = {};
            this.character.limits.x = {};
            this.character.limits.y = {};
            this.character.limits.x.min = BG_BORDERWEIGHT;
            this.character.limits.x.max = WIDTH_STAGE - BG_BORDERWEIGHT - WIDTH_CHARACTER;
            this.character.limits.y.min = BG_BORDERWEIGHT;
            this.character.limits.y.max = HEIGHT_STAGE - BG_BORDERWEIGHT - HEIGHT_CHARACTER;
            this.character.speed = 3;

            // Characters
            this.characterGroup = new Kiwi.Group(this);
            for(var i = 1; i < numPlayers; i++) {
                if(i == idRol) this.character.x = 50 + (i * WIDTH_CHARACTER * 3);
                else this.characterGroup.addChild(new Character(this, 50 + (i * WIDTH_CHARACTER * 3), 20));
            }
            if(idRol == 0) this.character.destroy();
            data.id = idRol;

            // Rocks
            this.rockGroup = new Kiwi.Group(this);
            for(var i = 0; i < rocksCoor.length; i++) {
                this.rockGroup.addChild(new Rock(this, rocksCoor[i].x, rocksCoor[i].y));
            }

            // Add to Game
            this.addChild(this.background);
            this.addChild(this.goal);
            this.addChild(this.characterGroup);
            this.addChild(this.rockGroup);
            this.addChild(this.character);
            this.addChild(this.gun);

            characters = this.characterGroup.members;
            rocks = this.rockGroup.members;
        }

        // Canvas' update
        actual.update = function() {
            Kiwi.State.prototype.update.call(this);

            if(!isDead && start) {
                if(idRol != 0) {
                    this.updateCharacter();
                    data.x = this.character.x;
                    data.y = this.character.y;
                }
                else {
                    this.updateGun();
                    data.x = this.gun.x;
                    data.y = this.gun.y;
                }
                match.emit("update", data);
            }

        }

        // Update gun (Only the same player))
        actual.updateGun = function() {
            this.gun.x = this.game.input.mouse.x - WIDTH_GUN / 2;
            this.gun.y = this.game.input.mouse.y - HEIGHT_GUN / 2;

            if(this.gun.x < this.gun.limits.x.min) this.gun.x = this.gun.limits.x.min;
            if(this.gun.x > this.gun.limits.x.max) this.gun.x = this.gun.limits.x.max;
            if(this.gun.y < this.gun.limits.y.min) this.gun.y = this.gun.limits.y.min;
            if(this.gun.y > this.gun.limits.y.max) this.gun.y = this.gun.limits.y.max;


            if(this.gun.animation.currentAnimation.name != "downloaded") {
                if(this.mouse.isDown) {
                    this.gun.animation.play("downloaded");
                    match.emit("isLoaded", false);

                    this.checkCollisionShot();

                    // Delay between shoots. Second Parameter is 4 * game.time.clock.units
                    this.timer = this.game.time.clock.createTimer('shoot', 4, 1, true);
                    this.timerEvent = this.timer.createTimerEvent(Kiwi.Time.TimerEvent.TIMER_COUNT, function() {
                        if(!isDead) {
                            match.emit("isLoaded", true);
                            this.gun.animation.play("loaded");
                        }
                    }, this);

                    this.game.input.mouse.reset();
                }
            }
        }

        // Update character (Only the same player)
        actual.updateCharacter = function() {
            if(this.upKey.isDown) {
                if(this.character.y > this.character.limits.y.min) this.character.y -= this.character.speed;
            }
            else if(this.downKey.isDown) {
                if(this.character.y < this.character.limits.y.max) this.character.y += this.character.speed;
            }
            else if(this.leftKey.isDown) {
                if(this.character.x > this.character.limits.x.min) this.character.x -= this.character.speed;
            }
            else if(this.rightKey.isDown) {
                if(this.character.x < this.character.limits.x.max) this.character.x += this.character.speed;
            }

            if(!processing) this.checkGoal();
        }

        // When the gun shot, detect collision
        actual.checkCollisionShot = function() {
            for(var i = 0; i < characters.length; i++) {
                var response = {};
                if(characters[i].box.bounds.intersects(this.gun.box.bounds)) {
                    var boundsGun = this.gun.box.bounds;
                    for(var j = 0; j < rocks.length; j++) {
                        if(rocks[j].box.bounds.intersects(boundsGun)) return;
                    }
                    console.log("Collision");
                    response.name = name;
                    response.id = i+1;
                    response.isPointGun = true;
                    match.emit("removePlayer", response);
                }
            }
        }

        // When the characters arrive to the goal
        actual.checkGoal = function() {
            if(this.character.box.bounds.intersects(this.goal.box.bounds)) {
                processing = true;
                console.log("Goal");
                var response = {};
                response.name = name;
                response.id = idRol;
                response.isPointGun = false;
                match.emit("removePlayer", response);
            }
        }

        // Another characters (Not me)
        var Character = function(state, x, y) {
            Kiwi.GameObjects.Sprite.call(this, state, state.textures.otherCharacter, x, y, false);

            this.speed = 5;
        }; Kiwi.extend(Character, Kiwi.GameObjects.Sprite);

        Character.prototype.update = function() {
            Kiwi.GameObjects.Sprite.prototype.update.call(this);
        };

        // Rocks
        var Rock = function(state, x, y) {
            Kiwi.GameObjects.Sprite.call(this, state, state.textures.rock, x, y, false);
        }; Kiwi.extend(Rock, Kiwi.GameObjects.Sprite);

        Rock.prototype.update = function() {
            Kiwi.GameObjects.Sprite.prototype.update.call(this);
        };

        myGame.states.addState(actual, true);

    }

});