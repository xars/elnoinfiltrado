// Config
var PORT = 3000;
var io = require('socket.io')(PORT);

// Constants
var WIDTH_STAGE = 700,
    HEIGHT_STAGE = 700,
    BG_BORDERWEIGHT = 10;

var MIN_PLAYERS = 3,
	MAX_PLAYERS = 7,
	TIME_MATCH = 30;

var endCounterGame = 0;
var numRocks = 0;
var deadPlayers = 0;
var users = [];
var players = [];
var numbers = [];
var usersLength = 0;
var gameOver = true;
var positions = {};
positions.characters = [];
positions.gun = {};

function setDefault() {
	endCounterGame = 0;
	numRocks = 0;
	deadPlayers = 0;
	players = [];
	numbers = [];
	gameOver = true;
	positions = {};
	positions.characters = [];
	positions.gun = {};
}

function updateUsersLength() {
	usersLength = 0;
	for(id in users) usersLength++;
}

// Return a Random Number no Repeat between 0 and <length>
// The Random Numbers will save in array <numbers>
function randomRol(length) {
	updateUsersLength();
	do {
		bool = false;
		aux = Math.floor(Math.random() * length);

		for(var j = 0; j < numbers.length; j++) {
			if(numbers[j] == aux) bool = true;
		}
	}while(bool && numbers.length != length);

	if(numbers.length != length) {
		numbers[numbers.length] = aux;
		return aux;
	}
	return -1;
}

var match = io
	.of('/match')
	.on('connection', function (socket) {
		console.log('a user connected');

		// Add to users[<name>] and save value of User's Socket.id
		socket.on('login', function(name) {
			if(users[name] == null) {
				users[name] = {};
				users[name].socketId = socket.id;
				users[name].idRol = -1;
				users[name].name = name;
				users[name].score = 0;
				match.to(socket.id).emit("loginSuccess");
			}
			else {
				match.to(socket.id).emit("alert", "El Usuario ya existe");
			}
		});


		// Add to players[<players.lenght>] and start counter for new game
		socket.on('joinGame', function(name) {
			// If he is the first player
			if(players.length == 0) {
				players[players.length] = name;
				respJoin = {};
				respJoin.time = 10; // Time for join a new game
				respJoin.is = "join";
				match.emit("startCounter", respJoin); // Start counter for join a new game

				setTimeout(function() {
					match.emit("endCounter");

					// If there are enough players
					if(players.length >= MIN_PLAYERS) {
						gameOver = false;
						deadPlayers = 0;

						// Rocks in the new Game
						numRocks = Math.floor((Math.random() * (players.length * 3)) + 1); // Rocks in the this game
						rocksCoor = []; // Rocks' coordinate
						for(var i = 0; i < numRocks; i++) {
							rocksCoor[i] = {};
							rocksCoor[i].x = Math.floor((Math.random() * (WIDTH_STAGE - (BG_BORDERWEIGHT*2) - 180)) + 100);
							rocksCoor[i].y = Math.floor((Math.random() * (HEIGHT_STAGE - (BG_BORDERWEIGHT*2) - 180)) + 100);
						}

						// Send an idRol(Random Number) for each player and send the players' length
						for(var i = 0; i < players.length; i++) {
							var inf = {};
							inf.rocksCoor = rocksCoor; // Rocks
							inf.numPlayers = players.length; // Number of Players
							inf.id = randomRol(players.length); // Random number (Rol)
							users[players[i]].idRol = inf.id; // Save idRol for each user
							if(inf.id != 0) { // Initialize variable for user's position
								positions.characters[inf.id] = {};
								positions.characters[inf.id].isDead = false;
							}
							match.to(users[players[i]].socketId).emit("newGame", inf);
						}

						// Start load screen and new game
						var respLoadScreen = {};
						respLoadScreen.time = 5; // Time of load screen
						respLoadScreen.is = "wait";
						match.emit("startCounter", respLoadScreen);
						match.emit("startGame", respLoadScreen.time);

						setTimeout(function() {
							match.emit("endCounter");
							var respNewGame = {};
							respNewGame.time = 30; // Game's time
							respNewGame.is = "start";
							match.emit("startCounter", respNewGame);

							// Fill score table
							var scoreTable = [];
							var index = -1;
							for(user in users) {
								scoreTable[++index] = {}
								scoreTable[index].name = users[user].name;
								scoreTable[index].score = users[user].score;
							}

							// Send score table
							match.emit("updateScore", scoreTable);

							// End Game. Send score table and game over
							endCounterGame = setTimeout(function() {
								if(!gameOver) {
									var scoreTable = [];
									var index = -1;
									for(user in users) {
										if(users[user].idRol == 0) {
											users[user].score += 5;
										}
										scoreTable[++index] = {}
										scoreTable[index].name = users[user].name;
										scoreTable[index].score = users[user].score;
									}
								}
								match.emit("updateScore", scoreTable);
								match.emit("endCounter");
								match.emit("gameOver");
								setDefault();
							}, respNewGame.time * 1000);
						}, respLoadScreen.time * 1000);

					}
					// If there are not enough players
					else {
						gameOver = true;
						setDefault();
						match.emit("alert", "Numero de Jugadores menor de 3");
					}

				}, respJoin.time * 1000); // End Counter for join a new game

			}
			// If he is not the first player
			else if(gameOver) {
				players[players.length] = name;
			}
			// If the game is not finished
			else {
				match.to(socket.id).emit("alert", "El Juego no ha Terminado"); // If somebody join into game and game has not finished
			}
		});

		// When a user disconnect
		socket.on('disconnect', function() {
			console.log('user disconnected');

			// Remove all thing of User
			for(name in users) {
				if(users[name].socketId == socket.id) {

					for(var i = 0; i < players.length; i++) {
						if(players[i] == name) {
							if(users[name].idRol != -1) {
								// If the user is sniper, game over
								if(users[name].idRol == 0) {
									match.emit("gameOver");
									match.emit("endCounter");
									clearTimeout(endCounterGame);
									setDefault();
								}
								else {
									if(players.length-1 >= 3) {
										positions.characters[users[name].idRol].isDead = true;
									}
									// If the user is runner and the players in game is not enough, game over
									else {
										match.emit("gameOver");
										match.emit("endCounter");
										clearTimeout(endCounterGame);
										setDefault();
									}
								}
							}
						}
					}


					for(var i = 0; i < numbers.length; i++) if(numbers[i] == users[name].idRol) numbers.splice(i,1);
					players.splice(name,1);
					delete users[name];
				}
			}
		});

		// Update Players' Positions
		socket.on('update', function(data) {
			if(data.id == 0) {
				if(positions.gun != undefined) {
					positions.gun.x = data.x;
					positions.gun.y = data.y;
				}
			}
			else {
				if(positions.characters[data.id] != undefined) {
					positions.characters[data.id].x = data.x;
					positions.characters[data.id].y = data.y;	
				}
			}

			match.emit("update", positions);
		});

		// When a player win or lose
		socket.on('removePlayer', function(response) {
			if(response.isPointGun) {
				users[response.name].score++;
			}
			else {
				users[response.name].score += (players.length-1) - deadPlayers;
			}
			var scoreTable = [];
			var index = -1;
			for(user in users) {
				scoreTable[++index] = {}
				scoreTable[index].name = users[user].name;
				scoreTable[index].score = users[user].score;
			}

			positions.characters[response.id].isDead = true;
			deadPlayers++;
			isFinishedGame = true;
			for(var i = 1; i < positions.characters.length; i++) {
				if(!positions.characters[i].isDead) isFinishedGame = false;
			}
			if(isFinishedGame) {
				match.emit("gameOver");
				match.emit("endCounter");
				clearTimeout(endCounterGame);
				setDefault();
			}

			setTimeout(function() {
				match.emit("updateScore", scoreTable);
			}, 2000);
		});

		// When the gun is loaded
		socket.on('isLoaded', function(boolean) {
			match.emit("isLoaded", boolean);
		});
	});